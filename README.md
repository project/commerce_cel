# Commerce Cobros en Línea

This project integrates [Cobros en Línea][0] payment services (formerly
RCCPagos) into the [Drupal Commerce][1] payment and checkout systems. Cobros en
Línea provides off-site payment for Argentina.  After confirmation the user will
be redirected to the Cobros en Línea website so no payment information is
collected by the module.

## Installation

Just download and enable the module and its dependencies as usual, no external
libraries needed.

## Configuration

TODO

[0]: http://www.rccpagos.coop                    "Cobros en Línea"
[1]: https://www.drupal.org/project/commerce     "Drupal Commerce"
