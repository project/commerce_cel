<?php

/**
 * @file
 * Implements Cobros en Línea payment services for use with Drupal Commerce.
 */

require_once 'includes/commerce_cel.payment_method.inc';

define('COMMERCE_CEL_SERVER', 'https://www.rccpagos.coop/servicios.php');

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_cel_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_cel'] = array(
    'base' => 'commerce_cel',
    'title' => t('Cobros en Línea'),
    'short_title' => t('Cobros en Línea'),
    'description' => t('Payment through Cobros en Línea services'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
    'file' => 'includes/commerce_cel.payment_method.inc',
  );

  return $payment_methods;
}

/**
 * Implements hook_permission().
 */
function commerce_cel_permission() {
  $permissions = array();

  $permissions['use cobros en linea in local mode'] = array(
    'title' => t('Use Cobros en Línea in local mode'),
    'description' => t('This allows the user to simulate payments through Cobros en Línea.'),
    'restrict access' => TRUE,
  );

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function commerce_cel_menu() {
  $items = array();

  $items['commerce_cel/local/%commerce_order'] = array(
    'title' => 'Cobros en Línea simulate responses',
    'page callback' => 'commerce_cel_local',
    'page arguments' => array(2),
    'access arguments' => array('use cobros en linea in local mode'),
    'type' => MENU_CALLBACK,
    'file path' => drupal_get_path('module', 'commerce_cel') . '/includes',
    'file' => 'commerce_cel.pages.inc',
  );

  return $items;
}


/**
 * Calculates the checksum of the request.
 *
 * @param array $data
 *   Data to be sent to Cobros en Línea.
 * @param array $payment_method
 *   The payment method instance.
 */
function commerce_cel_checksum($data, $payment_method) {
  // The verification string is obtained by concatenating al the fields in
  // alphabetical order with the secret key at the end, and applying an md5
  // sum to the result.
  ksort($data);
  $data[] = $payment_method['settings']['secret_key'];
  $checksum = strtolower(md5(implode('', $data)));
  return $checksum;
}

/**
 * Validates the Cobros en Línea verification string for this order.
 *
 * @param object $order
 *   The order being payed.
 * @param array $payment_method
 *   The payment method instance.
 * @param string $verification
 *   The verification string from Cobros en Línea.
 */
function commerce_cel_valid_verification($order, $payment_method, $verification) {
  $checksum = $order->data['commerce_cel']['checksum'];
  $key = $payment_method['settings']['secret_key'];
  $local_verif = strtolower(md5($checksum . $key));
  return $local_verif == $verification;
}


/**
 * Maps Cobros en Línea error codes to error messages.
 */
function commerce_cel_error_message($error_code) {
  // Error code to message mapping. Error codes 8 to 17 are too technical to
  // show in the page, so we default to a generic message.
  switch ($error_code) {
    case '3':
      return t('Exceeds card limit');

    case '4':
      return t('Expired card');

    case '5':
      return t('Invalid security code');

    case '6':
      return t('Invalid card');

    case '7':
      return t('The processor rejected the transaction');

    default:
      return t('Error processing the transaction');
  }
}
